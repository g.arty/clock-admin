package main

import (
	"flag"
	"fmt"
	"os"

	"clock-system/config"
	"clock-system/param"
	"clock-system/server"
	"clock-system/storage"

	"github.com/sirupsen/logrus"
)

var (
	filePath string
	help     bool
)

func usage() {
	fmt.Fprintf(os.Stdout, `clock - simlpe scheduler
Usage: clock [-h help] [-c ./config.yaml]
Options:
`)
	flag.PrintDefaults()
}

func main() {
	flag.StringVar(&filePath, "c", "./config.yaml", "Место файла конфигурации")
	flag.BoolVar(&help, "h", false, "help")
	flag.Usage = usage
	flag.Parse()
	if help {
		flag.PrintDefaults()
		return
	}


	config.SetConfig(filePath)
	param.SetStatic()
	storage.SetDb()

	defer storage.Db.Close()
	address := config.Config.GetString("server.host")
	if address == "" {
		logrus.Fatal("cannot find any server host config")
	}

	engine, err := server.CreateEngine()
	if err != nil {
		logrus.Fatal(err)
	}

	if e := engine.Start(address); e != nil {
		logrus.Fatal(e)
	}

}
