import directive from './directives'

const importDirective = Vue => {
  /**
   * v-draggable="options"
   * options = {
   *  trigger: /CSS/,
   *  body:    /CSS/,
   *  recover: //
   * }
   */
  Vue.directive('draggable', directive.draggable)
  /**
   * clipboard v-draggable="options"
   * options = {
   *  value:    /v-model/
   *
   */
  Vue.directive('clipboard', directive.clipboard)
}

export default importDirective
