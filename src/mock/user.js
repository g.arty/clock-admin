import Mock from 'mockjs'
import { doCustomTimes } from '@/libs/util'
const Random = Mock.Random

export const getMessageInit = () => {
  let unreadList = []
  doCustomTimes(3, () => {
    unreadList.push(Mock.mock({
      title: Random.cword(10, 15),
      create_time: '@date',
      msg_id: Random.increment(100)
    }))
  })
  let readedList = []
  doCustomTimes(4, () => {
    readedList.push(Mock.mock({
      title: Random.cword(10, 15),
      create_time: '@date',
      msg_id: Random.increment(100)
    }))
  })
  let trashList = []
  doCustomTimes(2, () => {
    trashList.push(Mock.mock({
      title: Random.cword(10, 15),
      create_time: '@date',
      msg_id: Random.increment(100)
    }))
  })
  return {
    unread: unreadList,
    readed: readedList,
    trash: trashList
  }
}

export const getContentByMsgId = () => {
  return `<divcourier new',="" monospace;font-weight:="" normal;font-size:="" 12px;line-height:="" 18px;white-space:="" pre;"=""><div>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: medium;">Содержание сообщения，Этот контент используется<span style="color: rgb(255, 255, 255); background-color: rgb(28, 72, 127);">Редактор расширенного текста</span>Отредактировано. Вы можете увидеть некоторый<span style="text-decoration-line: underline; font-style: italic; color: rgb(194, 79, 74);">формат</span></span></div><ol><li>Вы можете просмотреть формат данных, возвращенный макетом，и интерфейс, запрошенный Api для определения разработки вашего внутреннего интерфейса</li><li>После использования вашего реального интерфейса интерфейсную страницу в принципе не нужно изменять для удовлетворения основных потребностей.</li><li>Попробуйте это</li></ol><p>${Random.csentence(100, 200)}</p></divcourier>`
}

export const hasRead = () => {
  return true
}

export const removeReaded = () => {
  return true
}

export const restoreTrash = () => {
  return true
}

export const messageCount = () => {
  return 3
}
