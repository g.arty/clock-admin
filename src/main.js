// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import iView from 'iview'
import i18n from '@/locale'
import config from '@/config'
import importDirective from '@/directive'
import { directive as clickOutside } from 'v-click-outside-x'
import installPlugin from '@/plugin'
import './index.less'
import '@/assets/icons/iconfont.css'
import TreeTable from 'tree-table-vue'
import VOrgTree from 'v-org-tree'
import 'v-org-tree/dist/v-org-tree.css'
import LogViewer from '@femessage/log-viewer'
import VueHighlightJS from '@highlightjs/vue-plugin'
import 'highlight.js/styles/default.css'
// Highlight.js languages (Only required languages)
import shell from 'highlight.js/lib/languages/shell'

// Login
import 'vue-particle-line/dist/vue-particle-line.css'
import vueParticleLine from 'vue-particle-line'

// Mock
/* eslint-disable */
if (process.env.NODE_ENV !== 'production') require('@/mock')

Vue.use(vueParticleLine)
Vue.use(iView, {
  i18n: (key, value) => i18n.t(key, value)
})
Vue.use(TreeTable)
Vue.use(VOrgTree)
Vue.use(VueHighlightJS, {
  // Register only languages that you want
  languages: {
    shell,
  }
})

/**
 * @description Встроенный плагин администратора регистрации
 */
installPlugin(Vue)
/**
 * @description Советы по отключению производственной среды
 */
Vue.config.productionTip = false
/**
 * @description Конфигурация приложения для глобальной регистрации
 */
Vue.prototype.$config = config
/**
 * Инструкция по регистрации
 */
importDirective(Vue)
Vue.directive('clickOutside', clickOutside)

Vue.component(LogViewer.name, LogViewer)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  store,
  render: h => h(App)
})
