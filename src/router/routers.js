import Main from '@/components/main'
// import parentView from '@/components/parent-view'

/**
 * iview-admin-meta:
 * meta: {
 *  title: { String|Number|Function }
 *  hideInBread: (false) true
 *  hideInMenu: (false) true
 *  notCache: (false) true name
 *  access: (null)
 *  icon: (-)
 *  beforeCloseName: (-) tab '@/router/before-close.js'
 * }
 */

export default [
  {
    path: '/login',
    name: 'login',
    meta: {
      title: 'Login - Войти',
      hideInMenu: true
    },
    component: () => import('@/view/login/login.vue')
  },
  {
    path: '/',
    name: '_home',
    redirect: '/home',
    component: Main,
    children: [
      {
        path: '/home',
        name: 'home',
        meta: {
          // hideInMenu: true,
          title: 'Персональный центр',
          notCache: true,
          icon: 'md-home'
        },
        component: () => import('@/view/single-page/home')
      }
    ]
  },
  {
    path: '/container',
    name: 'container',
    component: Main,
    children: [
      {
        path: 'container-list',
        name: 'container-list',
        meta: {
          icon: 'md-git-pull-request',
          title: 'Список контейнеров'
        },
        component: () => import('@/view/container/list.vue')
      },
      {
        path: 'container-config',
        name: 'container-config',
        meta: {
          icon: 'md-analytics',
          title: 'Конфигурация контейнера',
          hideInMenu: true,
          hideInBread: true,
          notCache: true
        },
        component: () => import('@/view/container/config.vue')
      }
    ]
  },
  {
    path: '/task',
    name: 'task',
    component: Main,
    children: [
      {
        path: 'task-list',
        name: 'task-list',
        meta: {
          icon: 'md-repeat',
          title: 'Центр задач'
        },
        component: () => import('@/view/task/list.vue')
      }
    ]
  },
  {
    path: '/status',
    name: 'status',
    component: Main,
    children: [
      {
        path: 'status-index',
        name: 'status-index',
        meta: {
          icon: 'md-chatbubbles',
          title: 'Статус'
        },
        component: () => import('@/view/status/index.vue')
      }
    ]
  },
  {
    path: '/log',
    name: 'log',
    component: Main,
    children: [
      {
        path: 'log-list',
        name: 'log-list',
        meta: {
          icon: 'ios-bug',
          title: 'История'
        },
        component: () => import('@/view/log/list.vue')
      }
    ]
  },
  {
    path: '/error_logger',
    name: 'error_logger',
    meta: {
      hideInBread: true,
      hideInMenu: true
    },
    component: Main,
    children: [
      {
        path: 'error_logger_page',
        name: 'error_logger_page',
        meta: {
          icon: 'ios-bug',
          title: 'Сбор ошибок'
        },
        component: () => import('@/view/single-page/error-logger.vue')
      }
    ]
  },
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/500.vue')
  },
  {
    path: '*',
    name: 'error_404',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/404.vue')
  }
]
