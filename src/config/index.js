export default {
  /**
   * @description title
   */
  title: 'clock scheduled',
  /**
   * @description token Cookie
   */
  cookieExpires: 1,
  /**
   * @description false
   *              meta: {title: 'xxx'}
   *
   */
  useI18n: false,
  /**
   * @description api
   */
  baseUrl: {
    // dev: 'https://www.easy-mock.com/mock/5add9213ce4d0e69998a6f51/iview-admin/',
    dev: 'http://127.0.0.1:9528/v1',
    pro: 'v1'
  },

  wsHost: {
    dev: '127.0.0.1:9528/v1',
    pro: 'v1'
  },
  /**
   * @description home
   */
  homeName: 'home',

  plugin: {
    'error-store': {
      showInHeader: true, // false
      developmentOff: true // true
    }
  }
}
