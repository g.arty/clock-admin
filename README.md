<p align="center">
    <a href="https://www.iviewui.com">
        <img width="200" src="https://file.iviewui.com/logo-new.svg">
    </a>
</p>

<h1>
Admin MS
    <h3>Admin management system template</h3>
</h1>

[![](https://img.shields.io/travis/iview/iview-admin.svg?style=flat-square)]()
[![vue](https://img.shields.io/badge/vue-2.5.17-brightgreen.svg?style=flat-square)]()
[![iview ui](https://img.shields.io/badge/iview-3.2.2-brightgreen.svg?style=flat-square)]()

## Introduction
Admin MS is a front-end management background integration solution. It based on [Vue.js](https://github.com/vuejs/vue)

- [Features]()
- [Components]()
- [Form]()
- [Getting started]()
- [Build]()

![](public/screen1.png)
![](public/screen3.png)
![](public/screen2.png)

## Features
- Login / Logout
- Permission Authentication
- A list of filters
- Permission to switch

## Components
- Rich Text Editor
- Markdown Editor
- City Cascader
- Photos preview and edit
- Draggable list
- File upload
- Digital gradient
- Split-pane

## Form
- The article published
- Workflow
- Table
  - Drag-and-drop sort
  - Searchable form
  - Table export data
  - Export to Csv file
  - Export to Xls file
- Table to picture
  - Error Page
  - 403
  - 404
  - 500
- Router
  - Dynamic routing
  - With reference page
- Theme
- Shrink the sidebar
- Tag navigation
- Breadcrumb navigation
- Full screen / exit full screen
- Lock screen
- The message center
- Personal center

## Getting started
```bush
# clone the project
git clone https://gitlab.com/g.arty/clock-admin.git

// install dependencies
npm install

// develop
npm run dev
```

## Build
```bush
npm run build
```

## License
Copyright (c) 2021, garty09
