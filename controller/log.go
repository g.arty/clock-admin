package controller

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"clock-system/param"
	"clock-system/storage"
)


func GetLogs(c echo.Context) (err error) {
	var query storage.LogQuery

	resp := param.ApiResponse{
		Code: 200,
		Msg:  "success",
		Data: nil,
	}

	if err := c.Bind(&query); err != nil {
		resp.Msg = fmt.Sprintf("[get logs] error to get the query param with: %v", err)
		logrus.Error(resp.Msg)
		return c.JSON(http.StatusBadRequest, resp)
	}

	logs, err := storage.GetLogs(&query)
	if err != nil {
		resp.Msg = fmt.Sprintf("[get logs] error to get the logs: %v", err)
		logrus.Error(resp.Msg)
		return c.JSON(http.StatusBadRequest, resp)
	}

	page := param.ListResponse{
		Items:     logs,
		PageQuery: query,
	}

	resp.Data = page

	return c.JSON(http.StatusOK, resp)
}

func DeleteLogs(c echo.Context) error {
	var query storage.LogQuery

	resp := param.ApiResponse{
		Code: 200,
		Msg:  "success",
		Data: nil,
	}

	if err := c.Bind(&query); err != nil {
		resp.Msg = fmt.Sprintf("[delete logs] error to get the query param with: %v", err)
		logrus.Error(resp.Msg)
		return c.JSON(http.StatusBadRequest, resp)
	}

	go storage.DeleteLogs(&query)
	return c.JSON(http.StatusOK, resp)
}
