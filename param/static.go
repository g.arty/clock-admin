package param

import (
	"github.com/sirupsen/logrus"

	"clock-system/config"
)

var (
	DBConn = ""

	WebUser = ""
	WebPwd  = ""
	WebJwt  = ""
)


func SetStatic() {
	if tmp := config.Config.GetString("login.user"); tmp == "" {
		logrus.Fatal("empty login.user")
	} else {
		logrus.Println("[param] load user")
		WebUser = tmp
	}

	if tmp := config.Config.GetString("login.pwd"); tmp == "" {
		logrus.Fatal("empty login.user")
	} else {
		WebPwd = tmp
	}

	if tmp := config.Config.GetString("login.jwt"); tmp == "" {
		logrus.Fatal("empty login.jwt")
	} else {
		WebJwt = tmp
	}

	if tmp := config.Config.GetString("storage.conn"); tmp == "" {
		logrus.Fatal("empty login.user")
	} else {
		logrus.Println("[param] load DbConn")
		DBConn = tmp
	}

}
