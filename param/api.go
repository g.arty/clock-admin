package param


type (
	Page struct {
		Count int `query:"count" json:"count"`
		Index int `query:"index" json:"index"`
		Total int `json:"total"`
	}


	User struct {
		UserName string `json:"user_name"`
		UserPwd  string `json:"user_pwd"`
	}


	NodeQuery struct {
		Page
		KeyWord string `query:"keyword" json:"keyword"`
		TaskID  int    `query:"task_id" json:"task_id"`
	}
)


type (
	ApiResponse struct {
		Code int         `json:"code"`
		Msg  string      `json:"msg"`
		Data interface{} `json:"data"`
	}


	ListResponse struct {
		Items     interface{} `json:"items"`
		PageQuery interface{} `json:"page"`
	}

	RelationResponse struct {
		Nodes interface{} `json:"nodes"`
		Links interface{} `json:"links"`
	}
)
